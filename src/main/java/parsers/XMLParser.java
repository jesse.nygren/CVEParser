package parsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.opencsv.CSVReader;

import main.Data;

public class XMLParser implements Parser_IF {

	private int counter = 0;
	private int keywordHits = 0;

	public int getKeywordHits() {
		return keywordHits;
	}

	private DocumentBuilderFactory factory;
	private DocumentBuilder builder;

	public XMLParser() throws ParserConfigurationException {
		factory = DocumentBuilderFactory.newInstance();
		builder = factory.newDocumentBuilder();

	}

	@Override
	public List<Data> suodataXML(File xml, List<String> keywords) throws SAXException, IOException {
		String fileSource = xml.getName();
		List<Data> data = new ArrayList<Data>();
		
	

		// Mitrecase
		if (Objects.equals(fileSource, "mitre.xml")) {
			Document document = builder.parse(xml);
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;

					String mitreName = node.getAttributes().getNamedItem("name").getNodeValue();
					String mitreDesc = getString("desc", element);

					String inspectThis = mitreDesc.toLowerCase();

					for (String keyword : keywords) {
						int find = inspectThis.indexOf(keyword);

						if (find == -1) {
							// System.out.println(keyword + " NOT FOUND!");
						} else {
							//System.out.println(keyword + " FOUND!");
							// System.out.println(mitreDesc);
							counter++;
							data.add(new Data(mitreName, mitreDesc, ""));

							keywordHits++;

							break;

						}
					}
				}
			}
			System.out.println("Found entries from MITRE: " + keywordHits);
			counter = 0;
			return data;

		} else if (Objects.equals(fileSource, "nvd.xml") || Objects.equals(fileSource, "nvd2010.xml")
				|| Objects.equals(fileSource, "nvd2011.xml") || Objects.equals(fileSource, "nvd2012.xml")
				|| Objects.equals(fileSource, "nvd2013.xml") || Objects.equals(fileSource, "nvd2014.xml")
				|| Objects.equals(fileSource, "nvd2015.xml") || Objects.equals(fileSource, "nvd2016.xml")
				|| Objects.equals(fileSource, "nvd2017.xml") || Objects.equals(fileSource, "nvd2018.xml")) {
			Document document = builder.parse(xml);
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			counter = 0;

			for (int i = 0; i < nodeList.getLength(); i++) {

				Node node = nodeList.item(i);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;

					String nvdName = node.getAttributes().getNamedItem("id").getNodeValue();

					String nvdDesc = getString("vuln:summary", element);

					String inspectThis = nvdDesc.toLowerCase();

					for (String keyword : keywords) {
						int find = inspectThis.indexOf(keyword);

						if (find == -1) {

						} else {
							//System.out.println(keyword + " FOUND!");

							counter++;
							data.add(new Data(nvdName, nvdDesc, ""));

							keywordHits++;
							break;
						}
					}
				}
			}
			System.out.println("Found entries from NVD: " + keywordHits);
			counter = 0;
			return data;

		} else if (Objects.equals(fileSource, "CWE.csv")) {
			
			counter = 0;
			
			@SuppressWarnings("deprecation")
			CSVReader reader = new CSVReader(new FileReader(xml), ',');
			
			String[] record = null;
			
			while ((record = reader.readNext()) != null) {
				
				String name = record[1];
				String desc = record[4];
				String ext  = record[5];
				
				String inspectThis = name + " " + desc + " " + ext;
				
				for (String keyword : keywords) {
					int find = inspectThis.indexOf(keyword);

					if (find == -1) {

					} else {
						//System.out.println(keyword + " FOUND!");

						counter++;
						Data d = new Data(name, desc, ext);
						
						data.add(d);

						keywordHits++;
						break;
					}
				}
				
				
				
				
			}
			
			System.out.println("Found entries from CWE: " + keywordHits);
			counter = 0;
			
			reader.close();
			
			return data;

		} else {
			counter = 0;
			return null;
		}

	}

	public String getString(String tagName, Element element) {
		NodeList list = element.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			NodeList subList = list.item(0).getChildNodes();

			if (subList != null && subList.getLength() > 0) {
				return subList.item(0).getNodeValue();
			}
		}

		return null;
	}

	// Muokkaa CVE xml:än "kevyempään" muotoon jatkokäsittelyä ajatellen.
	@Override
	public List<Data> simplifyXML(File xml) throws SAXException, IOException {

		String fileSource = xml.getName();

		Document document = builder.parse(xml);
		List<Data> data = new ArrayList<Data>();
		NodeList nodeList = document.getDocumentElement().getChildNodes();

		if (Objects.equals(fileSource, "mitre.xml")) {

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;

					int index = counter++;

					String name = node.getAttributes().getNamedItem("name").getNodeValue();

					String status = element.getElementsByTagName("status").item(0).getChildNodes().item(0)
							.getNodeValue();

					String modDate = element.getElementsByTagName("phase").item(0).getChildNodes().item(0)
							.getNodeValue();

					String description = element.getElementsByTagName("desc").item(0).getChildNodes().item(0)
							.getNodeValue();

					// data.add(new Data(index, name, description));
				}
			}

			return data;
		} else if (Objects.equals(fileSource, "nvd2018.xml")) {

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;

					int index = counter++;

					String name = node.getAttributes().getNamedItem("name").getNodeValue();

					String status = element.getElementsByTagName("status").item(0).getChildNodes().item(0)
							.getNodeValue();

					String description = element.getElementsByTagName("desc").item(0).getChildNodes().item(0)
							.getNodeValue();

					// data.add(new Data(index, name, description));
				}
			}

			return data;

		} else {
			System.out.println("No such file");

			return null;
		}

	}

	// Kirjoittaa XML-tiedostoja, parametriksi simplifiedXML ja path (ja filen
	// nimi.xml)

	// Printtaa listan sisällön
	@Override
	public void printData(List<Data> data) {

		for (Data d : data) {
			// System.out.println(d.getIndex() + ".");
//			System.out.println(d.getName());
//			System.out.println(d.getDescription());
//			System.out.println(d.getExtension());
		}

	}

	@Override
	public List<Data> filterData(List<String> keywords) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void writeTextFile(List<Data> data, String name, List<String> keywords, int count) {

		// if (keywordHits > 0) {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd_MM_yyyy");
		LocalDateTime now = LocalDateTime.now();
		String filename = filename(name);

		BufferedWriter bw = null;
		FileWriter fw = null;

		int counter = 1;

		try {

			String content = name + " " + dtf.format(now) + "\n" + "\n";

			content += "Keyword hits: " + keywordHits + "\n" + "\n" + "ITERATION: " + count + "\n" + "\n";
			
			for (String key : keywords) {
				content += key + "\n";
			}

			for (Data d : data) {
				content += "==========================================" + "\n" + "\n" + "\n" + counter++ + ".   "
						+ d.getName() + "\n" + "\n" + d.getDescription() + "\n" + "\n" + d.getExtension() + "\n" + "\n";
			}

			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);
			bw.write(content);

			System.out.println("Word-file created: " + name);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}

		keywordHits = 0;


	}

	@Override
	public void createGSON(List<Data> data, String name) {

		List<String> gs = new ArrayList<String>();
		String content = "";

		try {
			Writer fw = new FileWriter(filename(name));
			BufferedWriter bw = new BufferedWriter(fw);

			for (int i = 0; i < data.size(); i++) {
				Data d = data.get(i);

				Gson gson = new Gson();

				String json = gson.toJson(d);

				gs.add(json);

			}

			for (String s : gs) {
				// System.out.println(s);

				content += System.lineSeparator() + s;
			}
			bw.write(content);
			bw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	@Override
	public void readGSON(File gsonFile) {

		try {
			BufferedReader br = new BufferedReader(new FileReader(gsonFile));
			String line = br.readLine();

			while (line != null) {
				//System.out.println(line);
				line = br.readLine();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JsonReader jsonReader = new JsonReader(new StringReader());

	}

	@Override
	public List<Data> filterGSON(List<String> keywords, File gsonFile, String path) {

		List<Data> data = new ArrayList<Data>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(gsonFile));
			String json = br.readLine();

			JsonParser parser = new JsonParser();

			String filtered = "";

			while (json != null) {

				//System.out.println(json);
				json = br.readLine();

				if (checkString(json, keywords) == true) {
					filtered += json + System.lineSeparator();

					JsonElement jsonTree = parser.parse(json);

					if (jsonTree.isJsonObject()) {
						JsonObject jsonObject = jsonTree.getAsJsonObject();

						JsonElement name = jsonObject.get("name");

						JsonElement description = jsonObject.get("description");

						JsonElement extension = jsonObject.get("extension");

						data.add(
								new Data(String.valueOf(name), String.valueOf(description), String.valueOf(extension)));

					}

				}

			}

			// FileUtils.deleteDirectory(gsonFile);

			Writer fw = new FileWriter(filename(path));
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(filtered);
			bw.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return data;
	}

	private boolean checkString(String gsonLine, List<String> keywords) {

		if (gsonLine != null) {

			String inspect = gsonLine;
			inspect.toLowerCase();

			for (String keyword : keywords) {

				int find = inspect.indexOf(keyword.toLowerCase());

				if (find == -1) {
					return false;
				} else {
					return true;

				}

			}
		}
		return false;

	}

	@Override
	public void writeXML(List<Data> data, String path) throws FileNotFoundException, UnsupportedEncodingException {
		// TODO Auto-generated method stub

	}

	private String filename(String name) {

		String filename = "data\\" + name + ".txt";

		return filename;

	}

}
