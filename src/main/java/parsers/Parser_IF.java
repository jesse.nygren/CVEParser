package parsers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.xml.sax.SAXException;

import main.Data;

public interface Parser_IF {
	
	public List<Data> suodataXML(File xml, List<String> keywords) throws SAXException, IOException;
	
	public List<Data> simplifyXML(File xml) throws SAXException, IOException;
	
	
	public void printData(List<Data> data);
	
	public List<Data> filterData(List<String> keywords);
	
	public void writeTextFile(List<Data> data, String source, List<String> keywords, int counter);
	
	
	public void writeXML(List<Data> data, String path) throws FileNotFoundException, UnsupportedEncodingException;
	
	public int getKeywordHits();
	
	public void createGSON(List<Data> data, String path);

	public List<Data> filterGSON(List<String> keywords, File gsonFile, String path);
	
	public void readGSON(File gsonFile);
	
}
