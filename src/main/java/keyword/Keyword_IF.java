package keyword;

import java.io.File;
import java.util.List;


public interface Keyword_IF {
	
	public void addKeyword(String word);
	
	public void deleteKeyword(String word);
	
	public void deleteAll();
	
	public void printKeywords();
	
	public List<String> getKeywords();
	
	public List<String> readFromFile(File file);
	
	public void printFilenames();
	
	public String selectFile(int selection);

}
