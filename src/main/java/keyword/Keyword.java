package keyword;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import main.Data;


public class Keyword implements Keyword_IF {

	private List<String> keys;
	private Iterator<String> itr;

	public Keyword() {
		keys = new ArrayList<String>();

	}

	@Override
	public List<String> getKeywords() {
		// TODO Auto-generated method stub
		return keys;
	}

	@Override
	public void addKeyword(String word) {
		System.out.println();
		keys.add(word);
		System.out.println(word + " was added!");
	}

	@Override
	public void deleteKeyword(String word) {

		itr = keys.iterator();

		while (itr.hasNext()) {
			if (itr.next().equals(word)) {
				itr.remove();
				System.out.println(word + " was removed!");
			} else {
				System.out.println(word + " was not found");
			}
		}

	}

	@Override
	public void deleteAll() {
		System.out.println();
		keys.clear();
		System.out.println("All deleted!");
		System.out.println();

	}

	@Override
	public void printKeywords() {
		System.out.println();
		for (String s : keys) {
			System.out.println(s);
		}
		System.out.println();

	}

	@Override
	public List<String> readFromFile(File file) {

		List<String> keys = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				keys.add(sCurrentLine.toLowerCase());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return keys;
	}

	@Override
	public void printFilenames() {
		File folder = new File("keywords\\");
		File[] listOfFiles = folder.listFiles();
		
		String[] filenames = new String[listOfFiles.length];

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				System.out.println(i + ". " + listOfFiles[i].getName());
				
			}
		}

		
		
	}

	@Override
	public String selectFile(int selection) {
		File folder = new File("keywords\\");
		File[] listOfFiles = folder.listFiles();
		String[] filenames = new String[listOfFiles.length];
		
		
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				filenames[i] = listOfFiles[i].getName();
			}
		}
		
		if (selection >= 0 && selection < listOfFiles.length) {
			System.out.println(filenames[selection] + " valittu!");
			return "keywords\\" + filenames[selection];
		} else {
			System.out.println("Virheellinen valinta.");
			return null;
		}
		
		
	}

}
