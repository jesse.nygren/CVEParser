package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import keyword.Keyword;
import keyword.Keyword_IF;
import parsers.Parser_IF;
import parsers.XMLParser;

public class Demo {

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {

		Parser_IF xmlParser = new XMLParser();



		Scanner sc = new Scanner(System.in);
		Keyword_IF keyW = new Keyword();
		int counter = 1;
		boolean firstIter = true;

		do {
			System.out.println();
			System.out.println("= = = = = = = = = = = = = = ");
			System.out.println("- Komennot - ");
			System.out.println("1 - Filtteröi data");
			System.out.println("2 - Lopeta");
			System.out.println();
			System.out.println("Kirjoita komento");
			System.out.print("> ");
			String input = sc.next();
			char menu = input.charAt(0);

			
			
			System.out.println();
			System.out.println("= = = = = = = = = = = = = = ");
			System.out.println();
			switch (menu) {

			case '1':

				System.out.println();
				String keywords = null;
				String varma = null;

				keyW.printFilenames();

				while (keywords == null) {
					System.out.println("Kirjoita keyword-tiedoston indeksi. ");
					System.out.print("> ");
					int keyword = sc.nextInt();
					keywords = keyW.selectFile(keyword);

					if (keywords != null) {
						System.out.println("Oletko varma valinnastasti? y / n");
						System.out.print("> ");
						varma = sc.next().toLowerCase();

						if (varma.equals("y")) {
							break;
						} else {
							keywords = null;
						}
					}
				}

				List<String> keylist = keyW.readFromFile(new File(keywords));
				System.out.println("Tulostetaanko keywordit? y / n");
				System.out.print("> ");
				String select = sc.next();

				if (select.toLowerCase().equals("y")) {
					for (String s : keylist) {
						System.out.println(s);
					}
				}

				System.out.println("Aloitetaanko filtteröinti? y / n");
				System.out.print("> ");
				select = sc.next();

				if (select.toLowerCase().equals("y")) {

					System.out
							.println("Syötä kuvaava nimitunniste suodatetuille tiedostoille. Esim päivämäärä ja aihe");
					System.out.print("> ");
					select = sc.next();
					System.out.println("");
					System.out.println("Filtteröinti aloitettu. Älä sulje ohjelmaa! Tämä saattaa kestää tovin!");
					System.out.println("");
					
					if(firstIter) {
						firstFilter(select, keylist, xmlParser,counter);
					} else {
						continuedFilter(select, keylist, xmlParser, counter);
					}
					
					System.out.println("");
					if (firstIter) {
						System.out.println(counter + ". filtteröinti alkuperäisestä aineistosta suoritettu!");
						counter++;
						firstIter = false;
					} else {
						System.out.println(counter + ". Filtteröinti suoritettu!");
						counter++;
					}
					System.out.println("Valitse 1 - jatkaaksesi iteroimista.");

				}

				break;

			case '2':
				System.out.println("BYE BYE!");
				System.exit(0);

			}

		} while (true);

	}

	private static void firstFilter(String name, List<String> keys, Parser_IF xmlParser, int counter)
			throws SAXException, IOException {
		nvdFirstFilter(name, keys, xmlParser, counter);
		mitreFirstFilter(name, keys, xmlParser, counter);
		cweFirstFilter(name, keys, xmlParser, counter);

	}

	private static void continuedFilter(String name, List<String> keys, Parser_IF xmlParser, int counter) {
		nvdContinuedFilter(name, keys, xmlParser, counter);
		mitreContinuedFilter(name, keys, xmlParser, counter);
		cweContinuedFilter(name, keys, xmlParser, counter);
	}

	private static void nvdFirstFilter(String name, List<String> keys, Parser_IF xmlParser, int counter)
			throws SAXException, IOException {
		String nvdFilepath = "cve\\NVD\\";

		List<Data> data;

		for (int i = 2010; i < 2019; i++) {
			String nvdFile = nvdFilepath + "nvd" + i + ".xml";

			data = xmlParser.suodataXML(new File(nvdFile), keys);
			xmlParser.writeTextFile(data, "WORD\\NVD\\" + name + " NVD" + i + " _Filtered from original_", keys, counter);

			xmlParser.createGSON(data, "JSON\\NVD\\NVD" + i + " JSON_FILTERED");

		}

	}

	private static void nvdContinuedFilter(String name, List<String> keys, Parser_IF xmlParser, int counter) {

		List<Data> data;

		for (int i = 2010; i < 2019; i++) {
			String jsonFile = "data\\JSON\\NVD\\NVD" + i + " JSON_FILTERED.txt";

			data = xmlParser.filterGSON(keys, new File(jsonFile), "JSON\\NVD\\NVD" + i + " JSON_FILTERED");

			xmlParser.writeTextFile(data, "WORD\\NVD\\" + name + " NVD" + i + " _Filtered more than once_", keys, counter);
		}

	}

	private static void mitreFirstFilter(String name, List<String> keys, Parser_IF xmlParser, int counter)
			throws SAXException, IOException {
		String mitreFilepath = "cve\\MITRE\\";
		List<Data> data = xmlParser.suodataXML(new File(mitreFilepath + "mitre.xml"), keys);

		xmlParser.writeTextFile(data, "WORD\\MITRE\\" + name + " MITRE " + " _Filtered from original_", keys, counter);

		xmlParser.createGSON(data, "JSON\\MITRE\\MITRE JSON_FILTERED");
	}

	private static void mitreContinuedFilter(String name, List<String> keys, Parser_IF xmlParser, int counter) {

		String jsonFile = "data\\JSON\\MITRE\\MITRE JSON_FILTERED.txt";

		List<Data> data = xmlParser.filterGSON(keys, new File(jsonFile), "JSON\\MITRE\\MITRE JSON_FILTERED");

		xmlParser.writeTextFile(data, "WORD\\MITRE\\" + name + " MITRE " + " _Filtered more than once_", keys, counter);

	}

	private static void cweFirstFilter(String name, List<String> keys, Parser_IF xmlParser, int counter)
			throws SAXException, IOException {
		String cweFilepath = "cve\\CWE\\";

		List<Data> data = xmlParser.suodataXML(new File(cweFilepath + "CWE.csv"), keys);

		xmlParser.writeTextFile(data, "WORD\\CWE\\" + name + " CWE " + " _Filtered from original_", keys, counter);

		xmlParser.createGSON(data, "JSON\\CWE\\CWE JSON_FILTERED");

	}

	private static void cweContinuedFilter(String name, List<String> keys, Parser_IF xmlParser, int counter) {

		String jsonFile = "data\\JSON\\CWE\\CWE JSON_FILTERED.txt";

		List<Data> data = xmlParser.filterGSON(keys, new File(jsonFile), "JSON\\CWE\\CWE JSON_FILTERED");

		xmlParser.writeTextFile(data, "WORD\\CWE\\" + name + " CWE " + " _Filtered more than once_", keys, counter);

	}

}
