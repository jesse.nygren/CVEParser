package main;

public class Data {

	
	//private int index;
	private String name;
	private String description;
	private String extension;

	public Data(String name, String description, String extension) {

	//	this.index = index;
		this.name = name;
		this.description = description;
		this.extension = extension;

	}

//	public int getIndex() {
//		return index;
//	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

//	public void setIndex(int index) {
//		this.index = index;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

//	@Override
//	public String toString() {
//		return "<Obj>" + "\n" + 
//				"<Index>" + index + "</Index>" + "\n" + 
//				"<Id>" + name + "</Id>" + "\n" + 
//				"<Desc>"+ "'" +  description  + "'" +"</Desc>" + "\n" + 
//				"<Ext>" + "'" + extension +  "'" +  "</Ext>" + "\n" + 
//				"</Obj>";
//	}

}
