package main;

import java.util.Scanner;

import keyword.Keyword;
import keyword.Keyword_IF;

public class Toolbox {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Keyword_IF keyW = new Keyword();

		do {
			System.out.println();
			System.out.println("= = = = = = = = = = = = = = ");
			System.out.println("- COMMANDS - ");
			System.out.println("1 - Add keyword");
			System.out.println("2 - Delete keyword");
			System.out.println("3 - Print keywords");
			System.out.println("4 - Delete all keywords");
			System.out.println("5 - Stop the program");
			System.out.println();
			System.out.println("Write your command");
			System.out.print("> ");
			String input = sc.next();
			char menu = input.charAt(0);

			System.out.println();
			System.out.println("= = = = = = = = = = = = = = ");
			System.out.println();
			switch (menu) {

			case '1':
				System.out.println("New keyword: ");
				System.out.println();
				System.out.print("> ");
				String key = sc.next();
				keyW.addKeyword(key);
				System.out.println();
				break;

			case '2':
				System.out.println("Delete keyword: ");
				System.out.println();
				System.out.print("> ");
				key = sc.next();
				keyW.deleteKeyword(key);
				System.out.println();
				break;

			case '3':
				System.out.println("Keywords:");
				keyW.printKeywords();
				System.out.println();
				break;

			case '4':
				System.out.println("All deleted");
				keyW.deleteAll();
				break;

			case '5':
				System.out.println("BYE BYE!");
				System.exit(0);

			}
			
		} while (true);

	}

}
