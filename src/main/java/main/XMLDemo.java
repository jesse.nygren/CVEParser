package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLDemo {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		// if (args.length != 1)throw new RuntimeException("The name of the XML file is
		// required!");

		Document doc;
		Element e = null;

		int counter = 0;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		DocumentBuilder builder = factory.newDocumentBuilder();

		PrintWriter writer1 = new PrintWriter("candidate.xml", "UTF-8");
		PrintWriter writer2 = new PrintWriter("entry.xml", "UTF-8");
		
		
		doc = builder.newDocument();

		File fXmlFile = new File("cve\\mitre.xml");
		Document document = builder.parse(fXmlFile);

		List<Data> data = new ArrayList<Data>();

		NodeList nodeList = document.getDocumentElement().getChildNodes();

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);

			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;

				int index = counter++;

				String name = node.getAttributes().getNamedItem("name").getNodeValue();

				String status = element.getElementsByTagName("status").item(0).getChildNodes().item(0).getNodeValue();

				String description = element.getElementsByTagName("desc").item(0).getChildNodes().item(0)
						.getNodeValue();

				data.add(new Data(index, name, status, description));

			}

		}

		for (Data d : data) {
			System.out.println(d.toString());
			
			
			
			if (Objects.equals(d.getStatus(), "Candidate")) {
				writer1.println(d.toString());
			} else {
				writer2.println(d.toString());
			}
			
			System.out.println();
		}

		writer1.close();
		writer2.close();
	}

}
