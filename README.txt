- - - - - cve Parser - - - - - 

CVE-tietokantojen parsimiseen tehty ohjelma. 
Ohjelma lukee tietokantoja: NVD(2010 - 2018 (XML)), MITRE (XML) ja CWE (CSV).



- - - Tietokannat - - - 
Tietokannat l�ytyv�t projektin hakemistosta: cveParser\cve


- - - Hakusanat - - - 

Hakusanat tallennetaan tekstitiedostona hakemistoon: cveParser\keywords

Hakusanat kannattaa kirjoittaa pienell�, vaikka ohjelma ei olekaan case sensitive. 

Hakusanat tulee kirjoittaa allekkain 

Esimerkki:

tietokone
prosessori
kikkihiiri
intternetti
inttersportti


Hakusanoja suunnitellessa kannattaa suunnitella my�s j�rjestys miten ne halutaan ajaa. 
Esim: halutaanko ensin rajata suurella m��r�ll� hakusanoja aineisto pienemm�ksi, jonka j�lkeen tarkennetaan supistunutta aineistoa
spesifimmill� hakusanoilla. 


- - - Tulokset - - -
Ohjelman tuottamat tulokset l�ytyv�t hakemistosta cveParser\data

Tulokset on kategorisoitu formaatin mukaan JSON ja WORD kansioihin.

JSON: JSON formaatissa oleva tekstitiedosto, jota k�ytet��n vain v�liaikaiseen tiedon s�il�miseen t�ss� ohjelmassa

WORD: Tulokset, joita saadaan ohjelmaa ajamalla. Suosittelen avaamaan wordilla tai wordpadilla.



- - - K�ytt� - - -


Ohjelman istunto alkaa aina puhtaalta p�yd�lt�, eli ohjelmaa k�ytett�ess� aletaan aina suodattamaan koko tietokantamateriaalsita. (HUOM ! **Hakusanojen k�yt�n ja suunittelun t�rkeys!**)

